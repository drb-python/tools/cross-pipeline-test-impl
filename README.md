# Pipelines to test all impl in main branch
This project is only to trigger all pipelines off all drb implementation.

## Stage of Pipeline


The stage Build and Complete are only to have a start and an end point

The Bridge stage are launch all in parallel, and they continue their execution even if one fails
At end the pipeline is OK only if all bridge are ok.

To see a specific triggered execution we can click on the corresponding Downstream stage

## Variables

Variables accept REGEX syntax (Do not enclose the regex in quotes)
The default values are defined in Settings/CI-CD  Variables section

![img_2.png](img_2.png)

### Variable MODULE

Default is special value: 'All' that mean that All implementation are included (EXCLUDE is applied in addition)

### Variable EXCLUDE

Default is value: 'none', due to no impl corresponding to ths regex, no impl are excluded by default. 

## Examples

### Example with EXCLUDE 

To launch all except java bridge

We can set EXCLUDE to ```java```

![img_1.png](img_1.png)

Due to MODULE is All by default, all bridge we be launched except java

### Example with MODULE list 

To launch only zip, tar and http

We can set MODULE to ```(zip|tar|http)```

![img.png](img.png)



